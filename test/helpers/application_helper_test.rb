require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Joshua's Microblogging App"
    assert_equal full_title("Help"), "Help | Joshua's Microblogging App"
  end
end
